__author__ = 'tan_nguyen and weili_nie'
import numpy as np

from three_layer_neural_network import NeuralNetwork
from three_layer_neural_network import generate_data

class Layer:
    def __init__(self, nn_input_dim, nn_neurons):
        '''
        :param nn_input_dim: input dimension
        :param nn_neurons: number of neurons in this layer
        '''
        self.nn_input_dim = nn_input_dim
        self.nn_neurons = nn_neurons

        # initialize the weights and biases in the network
        # np.random.seed(0)
        self.W = np.random.randn(self.nn_input_dim, self.nn_neurons) / np.sqrt(self.nn_input_dim)
        # self.W = np.ones((self.nn_input_dim, self.nn_neurons)) / np.sqrt(self.nn_input_dim)
        self.b = np.zeros((1, self.nn_neurons))

    def feedforward(self, a_pre, actFun):
        '''
        feedforward takes the input data and computes the output data in each layer
        :param a_pre: input data
        :param actFun: activation function
        :return: z, a
        '''

        # z_k = X*W_k + b_k (z1: m*n_H, X: m*2, W1: 2*n_H, b1: m*n_H)
        z = a_pre.dot(self.W) + self.b
        # a_k = f(z_k) (a1: m*n_H)
        a = actFun(z)
        return z, a

    def backprop(self, a_pre, delta):
        '''
        backprop implements backpropagation in each layer to compute the gradients used to update the parameters
        :param X: input data
        :param delta: given errors
        :return: dL/dW, dL/db
        '''
        dW = (a_pre.T).dot(delta)
        db = np.sum(delta, axis=0, keepdims=True)
        return dW, db


class DeepNeuralNetwork(NeuralNetwork):
    """
    This class builds and trains a neural network
    """

    def __init__(self, nn_input_dim, nn_hidden_dim_list, nn_out_actFun_dim, actFun_type='Tanh',
                 reg_lambda=0.01, seed=0):
        '''
        :param nn_input_dim: input dimension
        :param nn_hidden_dim_list: A list showing the hidden layer size from the first hidden layer to the last
        :param nn_out_actFun_dim: out_actFun dimension
        :param actFun_type: type of activation function. 3 options: 'tanh', 'sigmoid', 'relu'
        :param reg_lambda: regularization coefficient
        :param seed: random seed
        '''
        super().__init__(nn_input_dim, nn_hidden_dim_list[0], nn_out_actFun_dim, actFun_type, reg_lambda, seed)
        self.nn_hidden_dim_list = nn_hidden_dim_list
        self.nn_hidden_layers = len(self.nn_hidden_dim_list)
        np.random.seed(seed)

        # initialize the multi-layer neural network
        if self.nn_hidden_layers > 0:
            # create the input-to-hidden layer
            self.layers = [Layer(self.nn_input_dim, self.nn_hidden_dim_list[0])]

            print('Input-to-hidden Layer: {}*{}'.format(self.nn_input_dim, self.nn_hidden_dim_list[0]))

            # create hidden-to-hidden layers
            self.layers += [Layer(self.nn_hidden_dim_list[i],self.nn_hidden_dim_list[i+1])
                            for i in range(0, self.nn_hidden_layers-1)]

            for i in range(0, self.nn_hidden_layers-1):
                print('{}th hidden-to-hidden layer: {}*{}'.format(i+1, self.nn_hidden_dim_list[i],
                                                                  self.nn_hidden_dim_list[i+1]))

            # create hidden-to-output layer
            self.layers += [Layer(self.nn_hidden_dim_list[self.nn_hidden_layers-1], self.nn_out_actFun_dim)]

            print('Hidden-to-output layer: {}*{}'.format(self.nn_hidden_dim_list[self.nn_hidden_layers-1],
                                                         self.nn_out_actFun_dim))
            print('len of layers: {}'.format(len(self.layers)))
        else:
            # If we don't require hidden layers
            self.layers = [Layer(self.nn_input_dim, self.nn_out_actFun_dim)]

    def feedforward(self, X, actFun):
        '''
        feedforward builds a multi-layer neural network and computes the two probabilities,
        one for class 0 and one for class 1
        :param X: input data
        :param actFun: activation function
        :return:
        '''
        # compute z and a for the first layer
        self.list_z_a = [self.layers[0].feedforward(X, lambda x: self.actFun(x, type=self.actFun_type))]
        # compute z and a for the hidden layers
        for i in range(0, self.nn_hidden_layers):
            a_pre = self.list_z_a[i][1]
            self.list_z_a += [self.layers[i+1].feedforward(a_pre, lambda x: self.actFun(x, type=self.actFun_type))]
        # compute probs for the output layer
        exp_scores = np.exp(self.list_z_a[self.nn_hidden_layers][0])
        # (self.probs: m*2)
        self.probs = exp_scores / np.sum(exp_scores, axis=1, keepdims=True)
        self.list_z_a[self.nn_hidden_layers] = (self.list_z_a[self.nn_hidden_layers][0], self.probs)

        # for i in range(0, len(self.list_z_a)):
        #     print(' list_z_a {}: z={}, \n a={}'.format(i, self.list_z_a[i][0], self.list_z_a[i][1]))


    def calculate_loss(self, X, y):
        '''
        calculate_loss computes the loss for prediction
        :param X: input data
        :param y: given labels
        :return: the loss for prediction
        '''
        num_examples = len(X)
        self.feedforward(X, lambda x: self.actFun(x, self.actFun_type))
        # Calculating the loss

        # Calculate true labels
        Y = np.array([1-y, y]).T
        data_loss = -np.sum(Y * np.log(self.probs))

        # Add regulatization term to loss (optional)
        sum_W = 0
        for i in range(0, len(self.layers)):
            sum_W += np.sum(np.square(self.layers[i].W))
        data_loss += self.reg_lambda / 2 * sum_W
        return (1. / num_examples) * data_loss

    def backprop(self, X, y):
        '''
        backprop implements backpropagation in a multi-layer neural network
        :param X: input data
        :param y: given labels
        :return: list_dW_db
        '''
        # compute delta for the output layer
        num_examples = len(X)

        # delta2 : m*2
        delta = self.probs
        delta[range(num_examples), y] -= 1
        # compute delta for the hidden layers
        list_delta_rev = [delta]

        # print('list_delta_rev 0: {}'.format(list_delta_rev[0].shape))

        for i in range(0, self.nn_hidden_layers):
            delta_post = list_delta_rev[i]
            list_delta_rev += [self.diff_actFun(self.list_z_a[self.nn_hidden_layers-1-i][0], self.actFun_type) *
                               delta_post.dot(self.layers[self.nn_hidden_layers-i].W.T)]

        # for i in range(0, len(list_delta_rev)):
        #     print('list_delta_rev {}: {}'.format(i, list_delta_rev[i]))

        # compute dW and db for the output layer and hidden layers
        list_dW_db = []
        for i in range(0, self.nn_hidden_layers):
            delta_current = list_delta_rev[i]
            a_pre = self.list_z_a[self.nn_hidden_layers-1-i][1]
            list_dW_db += [self.layers[self.nn_hidden_layers - i].backprop(a_pre, delta_current)]
        list_dW_db += [self.layers[0].backprop(X, list_delta_rev[self.nn_hidden_layers])]
        list_dW_db.reverse()

        # for i in range(0, len(list_dW_db)):
        #     print('list_dW_db {}: {} and {}'.format(i, list_dW_db[i][0], list_dW_db[i][1]))

        return list_dW_db

    def fit_model(self, X, y, epsilon=0.01, num_passes=20000, print_loss=True):
        '''
        fit_model uses backpropagation to train the network
        :param X: input data
        :param y: given labels
        :param num_passes: the number of times that the algorithm runs through the whole dataset
        :param print_loss: print the loss or not
        :return:
        '''
        # Gradient descent.
        for i in range(0, num_passes):
            # Forward propagation
            self.feedforward(X, lambda x: self.actFun(x, type=self.actFun_type))
            # Backpropagation
            list_dW_db = self.backprop(X, y)
            list_dW = []
            list_db = []
            # Add regularization terms (b don't have regularization terms)
            list_dW += [list_dW_db[k][0] + self.layers[k].W*self.reg_lambda for k in range(0, len(list_dW_db))]
            list_db += [list_dW_db[k][1] for k in range(0, len(list_dW_db))]

            # Gradient descent parameter update
            for k in range(0, len(list_dW)):
                self.layers[k].W += -epsilon * list_dW[k]
                self.layers[k].b += -epsilon * list_db[k]

            # Optionally print the loss.
            # This is expensive because it uses the whole dataset, so we don't want to do it too often.
            if print_loss and (i+1) % 1000 == 0:
                # for k in range(0, len(list_dW)):
                #     print('W in layer{}: {}'.format(k, self.layers[k].W))
                #     print('b in layer{}: {}'.format(k, self.layers[k].b))
                print("Loss after iteration %i: %f" % (i+1, self.calculate_loss(X, y)))

def main():
    # # generate and visualize Make-Moons dataset
    X, y = generate_data()

    # plt.scatter(X[:, 0], X[:, 1], s=40, c=y, cmap=plt.cm.Spectral)
    # plt.show()

    model = DeepNeuralNetwork(nn_input_dim=2, nn_hidden_dim_list=[3, 3], nn_out_actFun_dim=2, actFun_type='Tanh')
    model.fit_model(X, y)
    model.visualize_decision_boundary(X, y)

if __name__ == "__main__":
    main()
